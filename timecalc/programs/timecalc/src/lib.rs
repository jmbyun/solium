use anchor_lang::prelude::*;
use anchor_lang::solana_program::entrypoint::ProgramResult;

declare_id!("FCjGngRaH4nBPEj9UbSAZMugKb5CuFoc33gAJFmyoWWV");

#[program]
pub mod timecalc {
    use super::*;

    pub fn refine(ctx: Context<Refine>, amount: u64) -> ProgramResult {
        let store: &mut Account<Store> = &mut ctx.accounts.store;
        let user: &Signer = &ctx.accounts.user;
        let clock: Clock = Clock::get().unwrap();

        store.user = *user.key;
        store.timestamp = clock.unix_timestamp;
        store.balance = amount;

        // return Err(ErrorCode::RefineryOccupied.into())

        Ok(())
    }
}

#[derive(Accounts)]
pub struct Refine<'info> {
    #[account(init, payer = user, space = Store::LEN)]
    pub store: Account<'info, Store>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program<'info, System>,
}

#[account]
pub struct Store {
    pub user: Pubkey,
    pub timestamp: i64,
    pub balance: u64,
}

const DISCRIMINATOR_LENGTH: usize = 8;
const PUBLIC_KEY_LENGTH: usize = 32;
const TIMESTAMP_LENGTH: usize = 8;
const UNSIGNED_64_LENGTH: usize = 8;
const STRING_LENGTH_PREFIX: usize = 4; // Stores the size of the string.
const MAX_TOPIC_LENGTH: usize = 50 * 4; // 50 chars max.
const MAX_CONTENT_LENGTH: usize = 280 * 4; // 280 chars max.

impl Store {
    const LEN: usize = DISCRIMINATOR_LENGTH
        + PUBLIC_KEY_LENGTH // User.
        + TIMESTAMP_LENGTH // Timestamp.
        + UNSIGNED_64_LENGTH; // Balance.
}

#[error_code]
pub enum ErrorCode {
    #[msg("The refinery is already occupied.")]
    RefineryOccupied,
}