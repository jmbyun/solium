const assert = require("assert");
const anchor = require("@project-serum/anchor");
const { SystemProgram } = anchor.web3;

describe("timecalc", () => {
  // Use a local provider.
  const provider = anchor.Provider.local();

  // Configure the client to use the local cluster.
  anchor.setProvider(provider);

  it("Refine raw materials", async () => {
    // #region code-simplified
    // The program to execute.
    const program = anchor.workspace.Timecalc;

    // The Account to create.
    const store = anchor.web3.Keypair.generate();

    // Create the new account and initialize it with the program.
    // #region code-simplified
    await program.rpc.refine(new anchor.BN(350), {
      accounts: {
        store: store.publicKey,
        user: provider.wallet.publicKey,
        systemProgram: SystemProgram.programId,
      },
      signers: [store],
    });
    // #endregion code-simplified

    // Fetch the newly created account from the cluster.
    const storeAccount = await program.account.store.fetch(store.publicKey);

    console.log(storeAccount);

    // Check it's state was initialized.
    assert.ok(storeAccount.balance.eq(new anchor.BN(350)));

    // // Store the account for the next test.
    // _myAccount = myAccount;
  });
});
