# Example: Bank

Saves numbers for accounts

## Development

### Test

To test the program with *mocha*, run the following command:

```
$ anchor test
```

### Deploy

To deploy the program, run the following command:

```
$ anchor build
```

and follow the message that looks like this:

```
To deploy this program:
  $ solana program deploy /Users/jeongmin/Workspace/solium/bank/target/deploy/bank.so
```

### Client

To run the client node application, run the following command to get your keypair first:

```
$ solana config get keypair
```

and then run the following command with your keypair file path:

```
$ ANCHOR_WALLET={KEYPAIR_FILE_PATH} node client.js
```
