const anchor = require("@project-serum/anchor");
const NodeWallet = require("@project-serum/anchor/dist/cjs/nodewallet").default;
const web3 = require("@solana/web3.js")
const { SystemProgram } = anchor.web3;

const programAddress = "EhodFoBYrJpVA8gKFaXzh78JAB491dSGURxzVeoUEuEw";

const commitment = "processed";

// const connection = new web3.Connection("https://api.devnet.solana.com", { commitment, wsEndpoint: "wss://api.devnet.solana.com/" });
const connection = new web3.Connection("http://127.0.0.1:8899", { commitment, wsEndpoint: "wss://127.0.0.1:8899/" });

// const options = anchor.Provider.defaultOptions();
// const userAccount = anchor.web3.Keypair.generate();
// const wallet = new NodeWallet(userAccount);
// const provider = new anchor.Provider(connection, wallet, options);

const provider = anchor.Provider.local();

anchor.setProvider(provider);

async function main() {
  // #region main
  // Read the generated IDL.
  const idl = JSON.parse(
    require("fs").readFileSync("./target/idl/bank.json", "utf8")
  );

  // Address of the deployed program.
  const programId = new anchor.web3.PublicKey(programAddress);

  // Generate the program client from IDL.
  const program = new anchor.Program(idl, programId);

  // The Accounts to create.
  const myAccount = anchor.web3.Keypair.generate();
  const tx2 = await connection.requestAirdrop(myAccount.publicKey, 1e9);
  
  // Execute the RPC.
  await program.rpc.initialize(new anchor.BN(1080), {
    accounts: {
      myAccount: myAccount.publicKey,
      user: provider.wallet.publicKey,
      systemProgram: SystemProgram.programId
    },
    signers: [myAccount],
  });

  const account = await program.account.myAccount.fetch(myAccount.publicKey);

  console.log("The stored value is: " + account.data.toNumber());
  // #endregion main
}

console.log("Running client.");
main().then(() => console.log("Success"));